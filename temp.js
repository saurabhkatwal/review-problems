let persons = [{
        name: 'John',
        grade: 8,
        Gender: 'M'
    },
    {
        name: 'Sarah',
        grade: 12,
        Gender: 'F'
    },
    {
        name: 'Bob',
        grade: 16,
        Gender: 'M'
    },
    {
        name: 'Johnny',
        grade: 2,
        Gender: 'M'
    },
    {
        name: 'Ethan',
        grade: 4,
        Gender: 'M'
    },
    {
        name: 'Paula',
        grade: 18,
        Gender: 'F'
    },
    {
        name: 'Donald',
        grade: 5,
        Gender: 'M'
    },
    {
        name: 'Jennifer',
        grade: 13,
        Gender: 'F'
    },
    {
        name: 'Courtney',
        grade: 15,
        Gender: 'F'
    },
    {
        name: 'Jane',
        grade: 9,
        Gender: 'F'
    },
    {
        name: 'John',
        grade: 17,
        Gender: 'M'
    },
    {
        name: 'Arya',
        grade: 14,
        Gender: 'F'
    },
];

let sortedGrades = persons.filter(person => {

    return person.name[0] === 'J' || person.name[0] === 'P';

}).sort((a, b) => {

    return b.grade - a.grade;

});

console.log("highest grade for people whose name starts with 'J' or 'P'");
console.log(sortedGrades[0].grade);

let highestGradesInMales = persons.filter(person => {

    return person.Gender === 'M';

}).sort((a, b) => {

    return b.grade - a.grade;

});

let highestGradeInMalesObj = highestGradesInMales[0];

console.log("highest grade among males is :");
console.log(highestGradeInMalesObj.grade);

// Find the highest grade for people whose name starts with 'J' or 'P'

// Find the highest grade in male


const fruitBasket = [
    'banana',
    'cherry',
    'orange',
    'apple',
    'cherry',
    'orange',
    'apple',
    'banana',
    'cherry',
    'orange',
    'fig',
];

/* 
Use the fruitBasket array to create an array of array. Each array will contain two values name of fruit and number of times
that fruit appeared. Use the variable defined above (fruitsObj). To get all the keys of an array you can use Object.keys()
Output: 
[['banana', 2], ['cherry', 3], ['orange', 3], ['apple', 2], ['fig', 1]]
*/
const uniqueFruitsList = fruitBasket.filter((fruit, index, arr) => {

    return arr.indexOf(fruit) === index;

})
let res = uniqueFruitsList.map(fruit => {

    let temp = [];

    temp.push(fruit);

    let fruitCount = fruitBasket.reduce((acc, fruitBasketFruit) => {
        return (fruitBasketFruit === fruit) ? (acc += 1) : (acc);
    }, 0);

    temp.push(fruitCount);

    return temp;
})

console.log(res);